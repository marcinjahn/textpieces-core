// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use itertools::Itertools;
use once_cell::sync::Lazy;
use proptest::prop_assert_eq;
use test_strategy::proptest;
use textpieces_core::collection;
use textpieces_foundations::ActionProvider;
use textpieces_foundations::ParameterType;
use textpieces_foundations::ParameterValue;
use textpieces_function_actions::Functions;

static ACTIONS: Lazy<Functions> = Lazy::new(collection::provider);

fn run_action(id: &str, parameters: &[ParameterValue], input: &str) -> Result<String, String> {
    smol::block_on(ACTIONS.perform_action(id, parameters, input)).map_err(|e| e.to_string())
}

macro_rules! test_action {
    ($id:literal, $(parameters: [ $($param:literal),+ ],)? input: $input:expr, output: $($output:tt)*) => {
        assert_eq!(
            run_action(
                $id,
                &[$($(ParameterValue::from($param)),+)?],
                $input
            ).map_err(|e| e.to_string()),
            __parse_output!($($output)*)
        );
    };
}

macro_rules! __parse_output {
    ($output:literal) => {
        Ok(String::from($output))
    };
    (Err($message:literal)) => {
        Err(String::from($message))
    };
    ($output:expr) => {
        $output
    };
}

#[ctor::ctor]
fn init() {
    textpieces_core::init()
}

mod actions {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn sha1() {
        test_action! {
            "sha1",
            input: "This text will be hashed",
            output: "90b882f98f3320fc86826b5bb02c529da327721e"
        }
    }

    #[test]
    fn sha256() {
        test_action! {
            "sha256",
            input: "This text will be hashed",
            output: "5e5f930847f683477640d3c6d0dc51ba1d7976b5171648a18937062ceeaaf9dc"
        }
    }

    #[test]
    fn sha384() {
        test_action! {
            "sha384",
            input: "This text will be hashed",
            output: "81c44cfdf8c10e582a1c79c35435ebf563990c9a3f90a35f\
                     9e01024f987c20fc5a8bd763e7ed468001c579c5a7b97394"
        }
    }

    #[test]
    fn sha512() {
        test_action! {
            "sha512",
            input: "This text will be hashed",
            output: "b993971af508fb9381e828e51f351cbd0f954ecacaf\
                     6bd7924f98451ce4d8b84a9446ed66f5e0ef2ab60ec\
                     8dcb4784eb656707a2214fe2acf7253dec41c61616"
        }
    }

    #[test]
    fn md5() {
        test_action! {
            "md5",
            input: "This text will be hashed",
            output: "eef5ad5e3f59459ffb8d418c94fc1b80"
        }
    }

    #[test]
    fn base64_encode() {
        test_action! {
            "base64-encode",
            input: "Some test text",
            output: "U29tZSB0ZXN0IHRleHQ="
        }
    }

    #[test]
    fn base64_decode() {
        test_action! {
            "base64-decode",
            input: "UnVzdCBpcyB0aGUgYmVzdCBsYW5ndWFnZQ==",
            output: "Rust is the best language"
        }

        test_action! {
            "base64-decode",
            input: "NotAValidBase64",
            output: Err("Invalid padding")
        }

        test_action! {
            "base64-decode",
            input: ",..,.tsratsrats.",
            output: Err("Invalid symbol 44, offset 0.")
        }

        test_action! {
            "base64-decode",
            input: "Tm90QUhl2WRVc2Vy",
            output: Err("invalid utf-8 sequence of 1 bytes from index 6")
        }
    }

    #[test]
    fn remove_trailing() {
        test_action! {
            "remove-trailing",
            input: "\n\n   SOME_TEXT   \n   MORE TEXT    \nTEXT\n\nTEXTEXEXTE\n\n\n",
            output: "\n\n   SOME_TEXT\n   MORE TEXT\nTEXT\n\nTEXTEXEXTE"
        }
    }

    #[test]
    fn trim_lines() {
        test_action! {
            "trim-lines",
            input: "\n\n   SOME_TEXT   \n   MORE TEXT    \nTEXT\n\nTEXTEXEXTE\n\n\n",
            output: "SOME_TEXT\nMORE TEXT\nTEXT\n\nTEXTEXEXTE"
        }
    }

    #[test]
    fn count_symbols() {
        test_action! {
            "count-symbols",
            input: "Какой-то случайный текст с не-ASCII символами\nИ переносами строки тоже",
            output: "70"
        }
    }

    #[test]
    fn count_lines() {
        test_action! {
            "count-lines",
            input: "it's\npretty\neasy\nto\ncount\nlines\nwithout\nfinal\n\nnewline",
            output: "10"
        }

        test_action! {
            "count-lines",
            input: "it's\npretty\nhard\nto\ncount\nlines\nwith\nfinal\n\nnewline\n",
            output: "11"
        }
    }

    #[test]
    fn count_words() {
        test_action! {
            "count-words",
            input: "  Use   really strange\n number of   white\tspace\
                    s and not only\u{A0}and skip trailing     \n\n",
            output: "13"
        }

        test_action! {
            "count-words",
            input: " \n\n\n\t\t     ",
            output: "0"
        }

        test_action! {
            "count-words",
            input: "word",
            output: "1"
        }
    }

    #[test]
    fn reverse_lines() {
        test_action! {
            "reverse-lines",
            input: "\n123\n456\n789\n019\n",
            output: "\n019\n789\n456\n123\n"
        }
    }

    #[test]
    fn reverse_text() {
        test_action! {
            "reverse-text",
            input: "\n123\n456\n789\n019\n",
            output: "\n910\n987\n654\n321\n"
        }
    }

    #[test]
    fn sort_lines() {
        test_action! {
            "sort-lines",
            input: "baba\ncabc\n\nzzzz\nzzzzz\nzababa",
            output: "\nbaba\ncabc\nzababa\nzzzz\nzzzzz"
        }
    }

    #[test]
    fn sort_lines_reverse() {
        test_action! {
            "sort-lines-reverse",
            input: "baba\ncabc\n\nzzzz\nzzzzz\nzababa",
            output: "zzzzz\nzzzz\nzababa\ncabc\nbaba\n"
        }
    }

    #[test]
    fn filter_lines_substring() {
        test_action! {
            "filter-lines-substring",
            parameters: [
                "marker",
                false
            ],
            input: "Text with marker\n\
                    Text without marker\n\
                    Text really without m-a-r-k-e-r\n\
                    Some random text\n",
            output: "Text with marker\nText without marker"
        }

        test_action! {
            "filter-lines-substring",
            parameters: [
                r"\tmarker",
                true
            ],
            input: "Text with\tmarker\n\
                    Text without marker\n\
                    Text really without m-a-r-k-e-r\n\
                    Some random text\n",
            output: "Text with\tmarker"
        }
    }

    #[test]
    fn filter_lines_substring_reverse() {
        test_action! {
            "filter-lines-substring-reverse",
            parameters: ["marker", false],
            input: "Text with marker\n\
                    Text without marker\n\
                    Text really without m-a-r-k-e-r\n\
                    Some random text\n",
            output: "Text really without m-a-r-k-e-r\nSome random text\n"
        }

        test_action! {
            "filter-lines-substring-reverse",
            parameters: [r"\tmarker", true],
            input: "Text with\tmarker\n\
                    Text without marker\n\
                    Text really without m-a-r-k-e-r\n\
                    Some random text\n",
            output: "Text without marker\nText really without m-a-r-k-e-r\nSome random text\n"
        }
    }

    #[test]
    fn filter_lines_regex() {
        test_action! {
            "filter-lines-regex",
            parameters: [r"\b\w+\d+\b"],
            input: "Some line with text432\n\
                    Do not match\n\
                    string0321 in the start of line\n\
                    [Invalid regex in text??\n\
                    Never use T9 in real world\n\
                    \n\
                    Empty",
            output: "Some line with text432\n\
                     string0321 in the start of line\n\
                     Never use T9 in real world"
        }

        test_action! {
            "filter-lines-regex",
            parameters: ["[invalid regex"],
            input: "",
            output: Err("Parsing error at position 14: Invalid character class")
        }
    }

    #[test]
    fn filter_lines_regex_reverse() {
        test_action! {
            "filter-lines-regex-reverse",
            parameters: [r"\b\w+\d+\b"],
            input: "Some line with text432\n\
                    Do not match\n\
                    string0321 in the start of line\n\
                    [Invalid regex in text??\n\
                    Never use T9 in real world\n\
                    \n\
                    Empty",
            output: "Do not match\n[Invalid regex in text??\n\nEmpty"
        }

        test_action! {
            "filter-lines-regex",
            parameters: [r"[invalid regex"],
            input: "",
            output: Err("Parsing error at position 14: Invalid character class")
        }
    }

    #[test]
    fn remove_substring() {
        test_action! {
            "remove-substring",
            parameters: ["ea", false],
            input: "The black tea is the beast of the sea, I want to deal with this real seal without fear and steal it like a meal.",
            output: "The black t is the bst of the s, I want to dl with this rl sl without fr and stl it like a ml."
        }

        test_action! {
            "remove-substring",
            parameters: ["\\\"ea\\\"", true],
            input: r#"The black t"ea" is the beast of the s"ea", I want to deal with this real s"ea"l without fear and st"ea"l it like a meal."#,
            output: r#"The black t is the beast of the s, I want to deal with this real sl without fear and stl it like a meal."#
        }
    }

    #[test]
    fn replace_substring() {
        test_action! {
            "replace-substring",
            parameters: ["ea", "ee", false],
            input: "The black tea is the beast of the sea, I want to deal with this real seal without fear and steal it like a meal.",
            output: "The black tee is the beest of the see, I want to deel with this reel seel without feer and steel it like a meel."
        }

        test_action! {
            "replace-substring",
            parameters: ["e\ta", "ee\n", true],
            input: "The black tea is the be\tast of the sea, I want to de\tal with this real se\tal without fear and steal it like a meal.",
            output: "The black tea is the bee\nst of the sea, I want to dee\nl with this real see\nl without fear and steal it like a meal."
        }
    }

    #[test]
    fn remove_regex() {
        test_action! {
            "remove-regex",
            parameters: [r"(.)\1"],
            input: "A little bee is glad to see that this meeting guarantee to be free from any root of poor  sell.",
            output: "A lile b is glad to s that this mting guarant to be fr from any rt of prse."
        }
    }

    #[test]
    fn replace_regex() {
        test_action! {
            "replace-regex",
            parameters: [r"(.)\1", r"$0$0"],
            input: "A little bee is glad to see that this meeting guarantee to be free from any root of poor  sell.",
            output: "A littttle beeee is glad to seeee that this meeeeting guaranteeee to be freeee from any roooot of poooor    sellll."
        }
    }

    #[test]
    fn prettify_json() {
        test_action! {
            "prettify-json",
            input: r#"{  "some_key": 3, "another_one": null, "array": [1, 2, 3, true], "nested": { "a": "b" } }"#,
            output: "\
                {\n  \
                  \"another_one\": null,\n  \
                  \"array\": [\n    \
                    1,\n    \
                    2,\n    \
                    3,\n    \
                    true\n  \
                  ],\n  \
                  \"nested\": {\n    \
                    \"a\": \"b\"\n  \
                  },\n  \
                  \"some_key\": 3\n\
                }"
        }
    }

    #[test]
    fn minify_json() {
        test_action! {
            "minify-json",
            input: "\
                {\n  \
                  \"another_one\": null,\n  \
                  \"array\": [\n    \
                    1,\n    \
                    2,\n    \
                    3,\n    \
                    true\n  \
                  ],\n  \
                  \"nested\": {\n    \
                    \"a\": \"b\"\n  \
                  },\n  \
                  \"some_key\": 3\n\
                }",
            output: r#"{"another_one":null,"array":[1,2,3,true],"nested":{"a":"b"},"some_key":3}"#
        }
    }

    #[test]
    fn prettify_xml() {
        test_action! {
            "prettify-xml",
            input: "<?xml version=\"1.0\"?><Tests><Test TestId=\"0001\" TestType=\"CMD\">\
                    <Name>Convert number to string</Name><CommandLine>Examp1.EXE</CommandLine>\
                    <Input>1</Input><Output>One</Output></Test><Test TestId=\"0002\" TestType=\"CMD\">\
                    <Name>Find succeeding characters</Name><CommandLine>Examp2.EXE</CommandLine><Input>abc</Input>\
                    <Output>def</Output></Test><Test TestId=\"0003\" TestType=\"GUI\">\
                    <Name>Convert multiple numbers to strings</Name><CommandLine>Examp2.EXE /Verbose</CommandLine>\
                    <Input>123</Input><Output>One Two Three</Output></Test><Test TestId=\"0004\" TestType=\"GUI\">\
                    <Name>Find correlated key</Name><CommandLine>Examp3.EXE</CommandLine><Input>a1</Input>\
                    <Output>b1</Output></Test><Test TestId=\"0005\" TestType=\"GUI\"><Name>Count characters</Name>\
                    <CommandLine>FinalExamp.EXE</CommandLine><Input>This is a test</Input><Output>14</Output>\
                    </Test><Test TestId=\"0006\" TestType=\"GUI\"><!-- Comment --><Name>Another Test</Name>\
                    <CommandLine>Examp2.EXE</CommandLine><Input>Test Input</Input><Output>10</Output>\
                    <!-- Another comment --></Test></Tests>
            ",
            output: "\
                <?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
                <Tests>\n  \
                  <Test TestId=\"0001\" TestType=\"CMD\">\n    \
                    <Name>Convert number to string</Name>\n    \
                    <CommandLine>Examp1.EXE</CommandLine>\n    \
                    <Input>1</Input>\n    \
                    <Output>One</Output>\n  \
                  </Test>\n  \
                  <Test TestId=\"0002\" TestType=\"CMD\">\n    \
                    <Name>Find succeeding characters</Name>\n    \
                    <CommandLine>Examp2.EXE</CommandLine>\n    \
                    <Input>abc</Input>\n    \
                    <Output>def</Output>\n  \
                </Test>\n  \
                <Test TestId=\"0003\" TestType=\"GUI\">\n    \
                    <Name>Convert multiple numbers to strings</Name>\n    \
                    <CommandLine>Examp2.EXE /Verbose</CommandLine>\n    \
                    <Input>123</Input>\n    \
                    <Output>One Two Three</Output>\n  \
                </Test>\n  \
                <Test TestId=\"0004\" TestType=\"GUI\">\n    \
                    <Name>Find correlated key</Name>\n    \
                    <CommandLine>Examp3.EXE</CommandLine>\n    \
                    <Input>a1</Input>\n    \
                    <Output>b1</Output>\n  \
                </Test>\n  \
                <Test TestId=\"0005\" TestType=\"GUI\">\n    \
                    <Name>Count characters</Name>\n    \
                    <CommandLine>FinalExamp.EXE</CommandLine>\n    \
                    <Input>This is a test</Input>\n    \
                    <Output>14</Output>\n  \
                </Test>\n  \
                <Test TestId=\"0006\" TestType=\"GUI\">\n    \
                    <!-- Comment -->\n    \
                    <Name>Another Test</Name>\n    \
                    <CommandLine>Examp2.EXE</CommandLine>\n    \
                    <Input>Test Input</Input>\n    \
                    <Output>10</Output>\n    \
                    <!-- Another comment -->\n  \
                </Test>\n\
              </Tests>"
        }
    }

    #[test]
    fn json_to_yaml() {
        test_action! {
            "json-to-yaml",
            input: r#"{"another_one":null,"array":[1,2,3,true],"nested":{"a":"b"},"some_key":3}"#,
            output: "\
                another_one: null\n\
                array:\n\
                - 1\n\
                - 2\n\
                - 3\n\
                - true\n\
                nested:\n  \
                  a: b\n\
                some_key: 3\n\
            "
        }
    }

    #[test]
    fn yaml_to_json() {
        test_action! {
            "yaml-to-json",
            input: "\
                another_one: null\n\
                array:\n\
                - 1\n\
                - 2\n\
                - 3\n\
                - true\n\
                nested:\n  \
                  a: b\n\
                some_key: 3\n\
            ",
            output: "\
                {\n  \
                  \"another_one\": null,\n  \
                  \"array\": [\n    \
                    1,\n    \
                    2,\n    \
                    3,\n    \
                    true\n  \
                  ],\n  \
                  \"nested\": {\n    \
                    \"a\": \"b\"\n  \
                  },\n  \
                  \"some_key\": 3\n\
                }"
        }
    }

    #[test]
    fn opml_to_rss() {
        test_action! {
            "opml-to-rss-links",
            input: "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<opml version=\"2.0\">\n  <head>\n    \
                    <title>NewsFlash OPML export</title>\n  </head>\n  <body>\n    <outline title=\"rust\" text=\"rust\">\
                    \n      <outline title=\"Rust Blog\" text=\"Rust Blog\" xmlUrl=\"http://blog.rust-lang.org/feed.xml\" \
                    htmlUrl=\"https://blog.rust-lang.org/\" type=\"rss\" />\n    </outline>\n    \
                    <outline title=\"elementary\" text=\"elementary\">\n      \
                    <outline xmlUrl=\"https://blog.elementary.io/feed.xml\" text=\"elementary Blog\" type=\"rss\" \
                    htmlUrl=\"https://blog.elementary.io/\" title=\"elementary Blog\" />\n    </outline>\n    <outline \
                    text=\"gnome\" title=\"gnome\">\n      <outline htmlUrl=\"https://thisweek.gnome.org/\" \
                    text=\"This Week in GNOME\" type=\"rss\" xmlUrl=\"https://thisweek.gnome.org/index.xml\" \
                    title=\"This Week in GNOME\" />\n      <outline htmlUrl=\"https://blog.gtk.org/feed/\" \
                    title=\"GTK Development Blog\" text=\"GTK Development Blog\" type=\"rss\" \
                    xmlUrl=\"https://blog.gtk.org/feed/\" />\n      <outline xmlUrl=\"http://blogs.gnome.org/aday/feed/\" \
                    text=\"Alan Day&apos;s Blog\" htmlUrl=\"https://blogs.gnome.org/aday/feed/\" \
                    title=\"Alan Day&apos;s Blog\" type=\"rss\" />\n      <outline type=\"rss\" \
                    htmlUrl=\"https://blogs.gnome.org/chergert/feed/\" xmlUrl=\"https://blogs.gnome.org/chergert/feed/\" \
                    text=\"Chrisian Hergert&apos;s Blog\" title=\"Chrisian Hergert&apos;s Blog\" />\n      \
                    <outline xmlUrl=\"https://blogs.gnome.org/tbernard/feed/\" htmlUrl=\"https://blogs.gnome.org/tbernard/feed/\" \
                    text=\"Tobias Bernard&apos;s Blog\" type=\"rss\" title=\"Tobias Bernard&apos;s Blog\" />\n      \
                    <outline title=\"Alexander Mihailenko&apos;s Blog\" htmlUrl=\"https://blogs.gnome.org/alexm/feed/\" \
                    text=\"Alexander Mihailenko&apos;s Blog\" type=\"rss\" xmlUrl=\"https://blogs.gnome.org/alexm/feed/\" />\
                    \n      <outline type=\"rss\" xmlUrl=\"https://www.bassi.io/feeds/all.atom.xml\" title=\"Emmanuele Bassi&apos;s Blog\" \
                    htmlUrl=\"https://www.bassi.io/\" text=\"Emmanuele Bassi&apos;s Blog\" />\n    </outline>\n    <outline \
                    title=\"osdev\" text=\"osdev\">\n      <outline type=\"rss\" title=\"Writing an OS in Rust\" \
                    xmlUrl=\"https://os.phil-opp.com/rss.xml\" text=\"Writing an OS in Rust\" htmlUrl=\"https://os.phil-opp.com/\" \
                    />\n    </outline>\n  </body>\n</opml>",
            output: "\
                http://blog.rust-lang.org/feed.xml\n\
                https://blog.elementary.io/feed.xml\n\
                https://thisweek.gnome.org/index.xml\n\
                https://blog.gtk.org/feed/\n\
                http://blogs.gnome.org/aday/feed/\n\
                https://blogs.gnome.org/chergert/feed/\n\
                https://blogs.gnome.org/tbernard/feed/\n\
                https://blogs.gnome.org/alexm/feed/\n\
                https://www.bassi.io/feeds/all.atom.xml\n\
                https://os.phil-opp.com/rss.xml\
            "
        }
    }

    #[test]
    fn escape_string() {
        test_action! {
            "escape-string",
            input: "some\t\n ♡random\r\'\"\\",
            output: r#"some\t\n ♡random\r\'\"\\"#
        }
    }

    #[test]
    fn unescape_string() {
        test_action! {
            "unescape-string",
            input: r#"some\t\n ♡random\r\'\"\\"#,
            output: "some\t\n ♡random\r\'\"\\"
        }
    }

    #[test]
    fn escape_url() {
        test_action! {
            "escape-url",
            input: "some ♡ <text &? # with symbols на русском",
            output: "some%20%E2%99%A1%20%3Ctext%20%26%3F%20%23%20with%20symbols%20%D0%BD%D0%B0%20%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%BE%D0%BC"
        }
    }

    #[test]
    fn unescape_url() {
        test_action! {
            "unescape-url",
            input: "some%20%E2%99%A1%20%3Ctext%20%26%3F%20%23%20with%20symbols%20%D0%BD%D0%B0%20%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%BE%D0%BC",
            output: "some ♡ <text &? # with symbols на русском"
        }
    }

    #[test]
    fn escape_html() {
        test_action! {
            "escape-html",
            input: r#"some ♡ <text &? # with symbols на русском/>\"'"#,
            output: r"some ♡ &lt;text &amp;? # with symbols на русском&#x2F;&gt;\&quot;&#x27;"
        }
    }

    #[test]
    fn unescape_html() {
        test_action! {
            "unescape-html",
            input: r"some ♡ &lt;text &amp;? # with symbols на русском&#x2F;&gt;\&quot;&#x27;",
            output: r#"some ♡ <text &? # with symbols на русском/>\"'"#
        }
    }

    #[test]
    fn remove_duplicates() {
        test_action! {
            "remove-duplicates",
            parameters: [false, false],
            input: &vec![
                "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello",
                "World",
                "Hello",
                "World", "World",
                "Hello", "Hello"
            ].join("\n"),
            output: "Hello\nWorld\nHello\nWorld\nHello"
        }

        test_action! {
            "remove-duplicates",
            parameters: [false, true],
            input: &vec![
                "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello",
                "World",
                "Hello",
                "World", "World",
                "Hello", "Hello"
            ].join("\n"),
            output: "World\nHello"
        }

        test_action! {
            "remove-duplicates",
            parameters: [true, false],
            input: &vec![
                "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello",
                "World",
                "Hello",
                "World", "World",
                "Hello", "Hello"
            ].join("\n"),
            output: "Hello\nWorld"
        }

        test_action! {
            "remove-duplicates",
            parameters: [true, true],
            input: &vec![
                "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello",
                "World",
                "Hello",
                "World", "World",
                "Hello", "Hello"
            ].join("\n"),
            output: ""
        }
    }

    #[test]
    fn remove_unique() {
        test_action! {
            "remove-unique",
            parameters: [false, false],
            input: &vec![
                "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello",
                "World",
                "Hello",
                "World", "World",
                "Hello", "Hello"
            ].join("\n"),
            output: "Hello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nWorld\nWorld\nHello\nHello"
        }

        test_action! {
            "remove-unique",
            parameters: [false, true],
            input: &vec![
                "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello",
                "World",
                "Hello",
                "World", "World",
                "Hello", "Hello"
            ].join("\n"),
            output: "Hello\nWorld\nHello"
        }

        test_action! {
            "remove-unique",
            parameters: [false, true],
            input: &vec![
                "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello",
                "World",
                "Hello", "Hello",
                "World", "World",
                "Hello", "Hello"
            ].join("\n"),
            output: "Hello\nHello\nWorld\nHello"
        }

        test_action! {
            "remove-unique",
            parameters: [true, false],
            input: &vec![
                "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello",
                "World",
                "Non-Aboba",
                "Hello",
                "World", "World",
                "Hello", "Hello",
                "Aboba"
            ].join("\n"),
            output: "Hello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nHello\nWorld\nHello\nWorld\nWorld\nHello\nHello"

        }

        test_action! {
            "remove-unique",
            parameters: [true, true],
            input: &vec![
                "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello", "Hello",
                "World",
                "Hello",
                "World", "World",
                "Hello", "Hello"
            ].join("\n"),
            output: "Hello\nWorld"
        }
    }
}

#[proptest]
fn never_panics(input: String) {
    for (id, action) in smol::block_on(ACTIONS.load_actions()).unwrap() {
        let parameter_sets = action
            .parameters
            .iter()
            .map(|param| match param.type_ {
                ParameterType::Bool => [true, false].map(ParameterValue::Bool).to_vec(),
                ParameterType::Int => [-42, -1, 0, 1, 42].map(ParameterValue::Int).to_vec(),
                ParameterType::String => ["", "texttexttext", "1", "\\"]
                    .map(String::from)
                    .map(ParameterValue::String)
                    .to_vec(),
                ParameterType::Float => {
                    [-f64::INFINITY, -42.0, -1.0, 0.0, 1.0, 42.0, f64::INFINITY]
                        .map(ParameterValue::Float)
                        .to_vec()
                }
            })
            .multi_cartesian_product();

        #[allow(unused_must_use)]
        for parameter_set in parameter_sets {
            std::hint::black_box(run_action(&id, &parameter_set, &input));
        }
    }
}

#[proptest]
fn base64_reversive(input: String) {
    let encoded = run_action("base64-encode", &[], &input).unwrap();
    prop_assert_eq!(run_action("base64-decode", &[], &encoded), Ok(input));
}

#[proptest]
fn count_symbols_reversive(c: char, #[strategy(0usize..=1000)] count: usize) {
    prop_assert_eq!(
        run_action("count-symbols", &[], &c.to_string().repeat(count)),
        Ok(count.to_string())
    );
}

#[proptest]
fn escape_string_reversive(input: String) {
    let escaped = run_action("escape-string", &[], &input).unwrap();
    prop_assert_eq!(run_action("unescape-string", &[], &escaped), Ok(input));
}

#[proptest]
fn escape_html_reversive(input: String) {
    let escaped = run_action("escape-html", &[], &input).unwrap();
    prop_assert_eq!(run_action("unescape-html", &[], &escaped), Ok(input));
}

#[proptest]
fn escape_url_reversive(input: String) {
    let escaped = run_action("escape-url", &[], &input).unwrap();

    prop_assert_eq!(run_action("unescape-url", &[], &escaped), Ok(input));
}
