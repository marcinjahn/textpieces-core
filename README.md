<!--
SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# Text Pieces Core

This is the back-end and internal implementation of [Text Pieces](https://gitlab.com/liferooter/textpieces)
in a separate crates.

This design decision allows to create multiple different front-ends for Text Pieces,
potentially allowing to make it cross-platform without sacrificing nativity.
