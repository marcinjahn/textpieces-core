// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

//! This crate provides ready-to-use collection of action providers
//! and re-exports core Text Pieces crates.
//!
//! Make sure to call [`init`] in the beginning of your program
//! before producing any observable effects (including waiting)
//! if you want to use the library.

#![warn(
    missing_docs,
    clippy::missing_docs_in_private_items,
    clippy::missing_panics_doc
)]

pub mod collection;

pub use textpieces_foundations::{
    Action, ActionError, Actions, ModifyStorageError, Parameter, ParameterType, ParameterValue,
    StopHandle,
};

pub use textpieces_foundations as foundations;

pub use textpieces_function_actions::init;

/// This module re-exports core crates with action provider implementations.
pub mod providers {
    pub use textpieces_function_actions as functions;
    pub use textpieces_script_actions as scripts;

    pub use functions::Functions;
    pub use scripts::Scripts;
}
