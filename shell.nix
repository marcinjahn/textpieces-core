{ pkgs }:
pkgs.mkShell {
  packages = with pkgs; [
    cargo
    cargo-outdated
    rustc
    clippy
    rustfmt
    rust-analyzer
  ];
}
