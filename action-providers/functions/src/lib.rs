// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: MIT

//! This crate provides action provider type for function-based actions.
//!
//! If you want to use it, make sure you always call [`init`] in the beginning
//! of the program before doing anything else. If you use `textpieces-core`
//! crate, you can use `textpieces-core::init` that calls [`init`].

#![warn(
    missing_docs,
    clippy::missing_docs_in_private_items,
    clippy::missing_panics_doc
)]

mod action;
mod parameters;

pub use action::*;
pub use parameters::*;

use std::{
    collections::{HashMap, HashSet},
    sync::Mutex,
};

use crossmist::{KillHandle, StaticRef};
use scopeguard::ScopeGuard;
use textpieces_foundations::{Action, ActionProvider, ActionProviderStop, ParameterValue};

pub use anyhow;

/// This function must be called before doing anything that have
/// observable effects.
pub fn init() {
    crossmist::init()
}

/// Trait representing a collection of function-based actions.
///
/// It's used because it allows to require the collection to
/// be defined as `const`. It guarantees that the collection
/// is available by the pointer at any moment. It's required
/// because
pub trait FunctionCollection {
    /// Array of function-based actions.
    const COLLECTION: &'static [DynFunctionAction];
}

/// Action provider for function-based actions.
pub struct Functions {
    /// Function-based actions of the provider.
    actions: HashMap<String, &'static &'static dyn GenericFunctionAction>,

    /// Kill handles of all running actions.:w
    kill_handles: Mutex<HashSet<KillHandle>>,
}

impl Functions {
    /// Creates new action provider.
    pub fn new<C: FunctionCollection>() -> Self {
        Self {
            actions: C::COLLECTION
                .iter()
                .map(|action| (action.id().to_owned(), action))
                .collect(),
            kill_handles: Default::default(),
        }
    }
}

impl ActionProvider for Functions {
    type LoadActionsError = std::convert::Infallible;

    async fn load_actions(&self) -> Result<HashMap<String, Action>, Self::LoadActionsError> {
        Ok(self
            .actions
            .iter()
            .map(|(id, action)| {
                (
                    id.clone(),
                    Action {
                        name: action.name().to_owned(),
                        description: action.description().to_owned(),
                        parameters: action.parameters(),
                    },
                )
            })
            .collect())
    }

    async fn perform_action(
        &self,
        action_id: &str,
        parameters: &[ParameterValue],
        input: &str,
    ) -> Result<String, String> {
        let parameters = parameters.to_vec();
        let input = input.to_owned();

        #[crossmist::func]
        fn func_action_function(
            action: StaticRef<DynFunctionAction>,
            input: String,
            parameters: Vec<ParameterValue>,
        ) -> Result<String, String> {
            action
                .call_function(&input, &parameters)
                .map_err(|err| err.to_string())
        }

        let child = func_action_function
            .spawn_smol(
                unsafe { StaticRef::new_unchecked(self.actions[action_id]) },
                input,
                parameters,
            )
            .await
            .map_err(|err| err.to_string())?;

        let kill_handle = child.get_kill_handle();
        let guard = scopeguard::guard((), move |()| {
            let _ = kill_handle.kill();
        });

        let result = child.join().await.map_err(|err| err.to_string())?;

        ScopeGuard::into_inner(guard);

        result
    }
}

impl ActionProviderStop for Functions {
    fn stop(&self) {
        for kill_handle in self.kill_handles.lock().unwrap().drain() {
            let _ = kill_handle.kill();
        }
    }
}
