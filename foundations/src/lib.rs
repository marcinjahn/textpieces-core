// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: MIT

//! # Overview
//!
//! This crate provides core abstractions for [Text Pieces](https://gitlab.com/liferooter/textpieces) application.
//!
//! ## Action
//!
//! The central idea of this crate is **action**.
//!
//! Action is a text transformation that takes
//! input string and optionally some parameters and returns some output
//! string or an error.
//!
//! Parameters are action-specific options used to pass more information
//! to the action. Each action defines its own list of available
//! parameters with human-readable description. User should be asked for
//! their values when they tries to perform an action that accepts some
//! parameters.
//!
//! Actions are stored in action providers and
//! accessible through high-level API using [`Actions`].
//! Every action has some ID and some [metadata](`Action`),
//! such as name, description and list of parameters it accepts.
//!
//! ## Action provider
//!
//! **Traits:** [`ActionProvider`] and [`ActionProviderMut`]
//!
//! Action provider contains low-level blocking implementation
//! of loading, peforming and optionally modifying actions.
//!
//! You shouldn't directly call their methods. Instead you
//! should use high-level API.
//!
//! ## High-level API
//!
//! **Type:** [`Actions`]
//!
//! This crates also provides high-level asynchronous API
//! for working with actions. It's [`Actions`] type.
//! [`Actions`] is a wrapper around [`ActionProvider`]
//! instance. It automatically validates your requests and
//! stores actions metadata.
//!
//! This crates only provides abstractions, so implementation of
//! action providers is on its users. For ready-to-use action
//! providers, see:
//! - [Function-based action provider](https://gitlab.com/liferooter/textpieces/-/tree/main/actions/functions)
//! - [Script-based action provider](https://gitlab.com/liferooter/textpieces/-/tree/main/actions/scripts)
//! - [Action collection used in the application](https://gitlab.com/liferooter/textpieces/-/tree/main/action-collection)

#![warn(
    missing_docs,
    clippy::missing_docs_in_private_items,
    clippy::missing_panics_doc
)]

mod action;
mod actions;
mod parameter;
mod provider;

#[cfg(feature = "glib")]
mod glib;

pub use actions::modify::*;
pub use actions::perform::*;
pub use actions::stop::*;

pub use action::Action;
pub use actions::Actions;

pub use parameter::*;
pub use provider::*;
