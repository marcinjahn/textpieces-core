// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

//! This module contains implementation of [`glib::value::ToValue`]
//! and [`glib::value::FromValue`] for [`ParameterValue`].

use std::hint::unreachable_unchecked;

use crate::{ParameterType, ParameterValue};

use glib::value::{FromValue, ToValue, ValueTypeChecker};
use thiserror::Error;

impl ToValue for ParameterValue {
    fn to_value(&self) -> glib::Value {
        match self {
            Self::String(value) => value.to_value(),
            Self::Bool(value) => value.to_value(),
            Self::Int(value) => value.to_value(),
            Self::Float(value) => value.to_value(),
        }
    }

    fn value_type(&self) -> glib::Type {
        match self.type_() {
            ParameterType::String => glib::Type::STRING,
            ParameterType::Bool => glib::Type::BOOL,
            ParameterType::Int => glib::Type::I64,
            ParameterType::Float => glib::Type::F64,
        }
    }
}

/// [`GValue`] type checker for [`ParameterValue`].
///
/// [`GValue`]: `glib::Value`
pub struct ParameterValueTypeChecker;

/// Error type for [`ParameterValueTypeChecker`].
#[derive(Debug, Clone, Copy, Error)]
#[error("invalid `GValue` type")]
pub struct InvalidGValueType;

unsafe impl ValueTypeChecker for ParameterValueTypeChecker {
    type Error = InvalidGValueType;

    fn check(value: &glib::Value) -> Result<(), Self::Error> {
        match value.type_() {
            glib::Type::STRING | glib::Type::BOOL | glib::Type::I64 | glib::Type::F64 => Ok(()),
            _ => Err(InvalidGValueType),
        }
    }
}

unsafe impl<'a> FromValue<'a> for ParameterValue {
    type Checker = ParameterValueTypeChecker;

    unsafe fn from_value(value: &'a glib::Value) -> Self {
        match value.type_() {
            glib::Type::STRING => Self::String(value.get().unwrap_unchecked()),
            glib::Type::BOOL => Self::Bool(value.get().unwrap_unchecked()),
            glib::Type::I64 => Self::Int(value.get().unwrap_unchecked()),
            glib::Type::F64 => Self::Float(value.get().unwrap_unchecked()),
            _ => unreachable_unchecked(),
        }
    }
}
