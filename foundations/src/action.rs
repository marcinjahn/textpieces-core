// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: MIT

//! This module contains type for action metadata.

use crate::Parameter;

/// Action metadata.
///
/// Contains the description of the action.
/// Doesn't contains action's ID and logic.
#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
#[cfg_attr(feature = "glib", derive(glib::Variant))]
pub struct Action {
    /// Action name.
    ///
    /// Should be short and conform `Title Case`.
    pub name: String,

    /// Action description.
    ///
    /// Should be longer than name, but not too long.
    /// Should conform `Sentence case`.
    pub description: String,

    /// Names of the action's parameters.
    ///
    /// Parameter names should be short and conform `Title Case`.
    #[serde(default)]
    pub parameters: Vec<Parameter>,
}
