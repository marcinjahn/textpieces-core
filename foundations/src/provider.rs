// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: MIT

//! This module contains [`ActionProvider`] and [`ActionProviderMut`] traits.
//!
//! These traits provides low-level API for working with actions.
//! You should use these traits to implement your own sources of actions
//! and use [`Actions`] instead to work with actions in your application.
//!
//! [`Actions`]: crate::Actions

use std::{collections::HashMap, error::Error, future::Future, marker::Send};

use crate::{Action, ParameterValue};

/// Action provider.
///
/// Provides low-level API for loading and performing actions.
///
/// This trait should only be used for implementing new providers
/// of actions. You are not supposed to call its methods directly.
pub trait ActionProvider {
    /// Error type for actions loading.
    type LoadActionsError: Error;

    /// Loads metadata of actions.
    fn load_actions(
        &self,
    ) -> impl Future<Output = Result<HashMap<String, Action>, Self::LoadActionsError>> + Send;

    /// Performs an action.
    ///
    /// # Guarantees
    /// [`Actions`] calls this function with following guarantees:
    /// - The action exists.
    /// - The number of passed parameters is correct.
    /// - Types of passed parameters are correct.
    ///
    /// These guarantees are not safety guarantees,
    /// so you shouldn't use `unsafe` that requires
    /// them to be followed.
    ///
    /// The future may be dropped before finishing
    /// is the action is cancelled.
    ///
    /// [`Actions`]: crate::Actions
    fn perform_action(
        &self,
        action_id: &str,
        parameters: &[ParameterValue],
        input: &str,
    ) -> impl Future<Output = Result<String, String>>;
}

/// Stoppable action provider.
///
/// Provides API for stopping all running actions.
///
/// Can be useful in situations where dropping all
/// running futures of the provider is not convenient
/// or even possible.
pub trait ActionProviderStop: ActionProvider {
    /// Stops all currenly running actions.
    ///
    /// This function is intentionally blocking
    /// because if you want to stop everything
    /// *right* now, you probably don't want to
    /// wait.
    fn stop(&self);
}

/// Mutable action provider.
///
/// Provides low-level API for saving modified actions.
///
/// This trait is supposed to be used only for implementing
/// new sources of actions. You should never call its methods directly.
pub trait ActionProviderMut: ActionProvider {
    /// Error type for modified actions saving.
    type SaveActionsError: Error;

    /// Updates action metadata.
    ///
    /// Next called [`ActionProvider::load_actions`] should
    /// return the new metadata.
    ///
    /// Note that this method only saves action metadata.
    /// Usually you'll have to do some provider-specific work
    /// to make new actions work or to completely delete removed ones.
    fn update_actions(
        &self,
        actions: &HashMap<String, Action>,
    ) -> impl Future<Output = Result<(), Self::SaveActionsError>> + Send;
}
